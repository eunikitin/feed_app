import React, { Component } from 'react';
import { connect } from 'react-redux';

import { fetchCurrentUser, fetchUsers } from '../../../actions/user';

import EventListContainer from '../../../containers/EventListContainer/index';
import UserList from '../../../components/UserList';


const mapStateToProps = (state) => ({
  users: state.indexReducer.user.fetchUsers.payload,
  currentUser: state.indexReducer.user.fetchCurrentUser.payload
});


class UserContainer extends Component {
  componentWillMount() {
    this.props.dispatch(fetchUsers());
    this.props.dispatch(fetchCurrentUser(this.props.params.id));
  }

  componentWillReceiveProps(props) {
    if(props.currentUser.id !== props.params.id)
      this.props.dispatch(fetchCurrentUser(props.params.id));
  }

  render() {
    return <div>
      <EventListContainer user={this.props.currentUser}/>
      <UserList users={this.props.users}/>
    </div>
  }
}

export default connect(mapStateToProps)(UserContainer)
