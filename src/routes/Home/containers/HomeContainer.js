import React, { Component } from 'react';
import { connect } from 'react-redux';

import { fetchCurrentUser, fetchUsers } from '../../../actions/user';

import UserList from '../../../components/UserList';

import './HomeContainer.scss';

const mapStateToProps = (state) => ({
  users: state.indexReducer.user.fetchUsers.payload,
  currentUser: state.indexReducer.user.fetchCurrentUser.payload
});


class UserContainer extends Component {
  componentWillMount() {
    this.props.dispatch(fetchUsers());
  }

  componentWillReceiveProps(props) {
    if(props.currentUser.id !== props.params.id)
      this.props.dispatch(fetchCurrentUser(props.params.id));
  }

  render() {
    return <div>
      <UserList users={this.props.users}/>
      <div className="home__content">
        <h1 className="home__title">Choose user from the list</h1>
      </div>
    </div>
  }
}

export default connect(mapStateToProps)(UserContainer)
