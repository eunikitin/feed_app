import React from 'react';
import PropTypes from 'prop-types';

import './UserList.scss';

import User from '../User';


class UserList extends React.Component {
  render() {
    return <div className="user-list">
      {this.props.users.map((user) =>
        <User key={user.id} id={parseInt(user.id)} avatar={user.avatar} username={user.username}/>
      )}
    </div>
  }
};

UserList.propTypes = {
  users: PropTypes.array,
}

export default UserList
