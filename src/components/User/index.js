import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';

import './User.scss';


class User extends React.Component {
  render() {
    return <Link className="user" activeClassName="active" to={'/user/' + this.props.id}>
      <img className="user__avatar" src={this.props.avatar}/>
      <span className="user__username">{this.props.username}</span>
    </Link>
  }
}


User.propTypes = {
  id: PropTypes.number.isRequired,
  username: PropTypes.string.isRequired,
  avatar: PropTypes.string.isRequired,
 }


export default User;
