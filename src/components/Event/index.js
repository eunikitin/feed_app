import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import moment from 'moment';

import './Event.scss';


class Event extends React.Component {

  render() {
    return <div className="event">
      <Link className="event__author" to={`/user/${this.props.author.id}`}>
        <img className="event__avatar" src={this.props.author.avatar}/>
        <span className="event__username">{this.props.author.username}</span>
      </Link>
      <div className="event__delete" onClick={() => { this.props.deleteEvent(this.props.id); }}>Delete</div>
      <div className="event__description">{this.props.description}</div>
      <span className="event__id-label">ID: <span className="event__id">{this.props.id}</span></span>
      <span className="event__date">{moment.unix(this.props.date).format("HH:mm DD.MM.YYYY")}</span>
    </div>
  }

}


Event.propTypes = {
  id: PropTypes.number.isRequired,
  description: PropTypes.string,
  author: PropTypes.object.isRequired,
  message: PropTypes.string,
  date: PropTypes.number
}


export default Event;
