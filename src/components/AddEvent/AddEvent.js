import React from 'react'
import PropTypes from 'prop-types';

import './AddEvent.scss'


class AddEvent extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      value: ''
    };
  }

  handleChange(e) {
    this.setState({value: e.target.value});
  }

  render() {
    return <div className="add-event">
      <span className="add-event__title">Add event</span>
      <textarea className="add-event__text" onChange={this.handleChange.bind(this)} value={this.state.value}/>
      <div className="btn btn-success add-event__submit" onClick={
        () => {
          this.props.addEvent(this.state.value);
          this.setState({value: ''});
        }
      }>Submit</div>
    </div>
  }
}


AddEvent.propTypes = {
  addEvent: PropTypes.func.isRequired,
}


export default AddEvent
