import React from 'react'
import DuckImage from '../assets/Duck.jpg'
import './HomeView.scss'
import { fetchEvents } from '../../../actions/event';

fetchEvents(1);

export const HomeView = () => (
  <div>
    <h4>Welcome!</h4>
    <img alt='This is a duck, because Redux!' className='duck' src={DuckImage} />
  </div>
)

export default HomeView
