import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import moment from 'moment';

import { fetchEvents, addEvent, deleteEvent } from '../../actions/event';

import Event from '../../components/Event/index';
import AddEvent from '../../components/AddEvent/AddEvent';

import './EventListContainer.scss';


const mapStateToProps = (state) => ({
  events : state.indexReducer.event.fetchEvents.payload
});


class EventListContainer extends Component {
  addEvent(description) {
    this.props.dispatch(addEvent({
      createdAt: moment().unix(),
      description: description
    }, this.props.user.id));
  }

  deleteEvent(id) {
    this.props.dispatch(deleteEvent(this.props.user.id, id));
  }

  componentWillReceiveProps(props) {
    if(this.props.user !== props.user ) {
      this.props.dispatch(fetchEvents(props.user.id));
    }
  }

  render() {

    return <div className="event-list">
      <AddEvent addEvent={this.addEvent.bind(this)}/>
      {
        this.props.events.reverse().map((event) => <Event
          key={event.id}
          id={parseInt(event.id)}
          author={this.props.user}
          description={event.description}
          date={event.createdAt}
          deleteEvent={this.deleteEvent.bind(this)}
        />)
      }
    </div>;
  }
}


EventListContainer.propTypes = {
  user: PropTypes.object.isRequired,
  events: PropTypes.array.isRequired,
}


export default connect(mapStateToProps)(EventListContainer);
