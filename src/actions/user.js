import {
  REQUEST_USERS,
  RECEIVE_USERS,
  FAILURE_USERS,

  REQUEST_CURRENT_USER,
  RECEIVE_CURRENT_USER,
  FAILURE_CURRENT_USER,
} from '../constants/user';


const url = 'http://5941464bf52e4800113b5f03.mockapi.io/';


export function fetchUsers () {
  return dispatch => {
    dispatch(requestUsers());
    return fetch(url + `/authors`, {
      method: 'GET',
    }).then(
      response => response.json(),
      err => dispatch(failureUsers(err))
    ).then(
      users => dispatch(receiveUsers(users))
    );
  }
}

export function requestUsers () {
  return {
    type: REQUEST_USERS
  }
}

export function receiveUsers (users) {
  return {
    type: RECEIVE_USERS,
    payload: users
  }
}

export function failureUsers (err) {
  return {
    type: FAILURE_USERS,
    error: err
  }
}


export function fetchCurrentUser (id) {
  return dispatch => {
    dispatch(requestCurrentUser());
    return fetch(url + `/authors/${id}`, {
      method: 'GET',
    }).then(
      response => response.json(),
      err => dispatch(failureCurrentUser(err))
    ).then(
      userInfo => dispatch(receiveCurrentUser(userInfo))
    );
  }
}

export function requestCurrentUser (id) {
  return {
    type: REQUEST_CURRENT_USER,
    payload: {
      id
    }
  }
}

export function receiveCurrentUser (currentUserInfo) {
  return {
    type: RECEIVE_CURRENT_USER,
    payload: currentUserInfo
  }
}

export function failureCurrentUser (err) {
  return {
    type: FAILURE_CURRENT_USER,
    error: err
  }
}