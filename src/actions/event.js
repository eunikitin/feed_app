
const url = 'http://5941464bf52e4800113b5f03.mockapi.io/';

import {
  REQUEST_EVENTS,
  RECEIVE_EVENTS,
  FAILURE_EVENTS,

  REQUEST_ADD_EVENT,
  SUBMIT_ADD_EVENT,
  FAILURE_ADD_EVENT,

  REQUEST_DELETE_EVENT,
  SUBMIT_DELETE_EVENT,
  FAILURE_DELETE_EVENT
} from '../constants/event';


export function fetchEvents (author) {
  return dispatch => {
    if(!author) throw new Error('Author id should be defined');
    dispatch(requestEvents(author));
    return fetch(url + `/authors/${author}/events/`, {
      method: 'GET',
    }).then(
      response => response.json(),
      err => dispatch(failureEvents(err))
    ).then(
      events => dispatch(receiveEvents(events))
    );
  }
}

export function requestEvents (author) {
  return {
    type: REQUEST_EVENTS,
    payload: {
      author,
    }
  }
}

export function receiveEvents (events) {
  return {
    type: RECEIVE_EVENTS,
    payload: events
  }
}

export function failureEvents (error) {
  return {
    type: FAILURE_EVENTS,
    payload: error
  }
}


export function addEvent(event, authorId) {
  return function (dispatch) {
    dispatch(requestAddEvent());
    return fetch(`${url}/authors/${authorId}/events`, {
      method: 'POST',
      body: JSON.stringify(event),
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
    }).then(
      data => data.json(),
      err => dispatch(failureAddEvent())
    ).then(
      events => dispatch(submitAddEvent(events))
    ).then(
      () => dispatch(fetchEvents(authorId))
    );
  }
}


export function requestAddEvent () {
  return {
    type: REQUEST_ADD_EVENT,
  }
}


export function submitAddEvent () {
  return {
    type: SUBMIT_ADD_EVENT,
  }
}

export function failureAddEvent (error) {
  return {
    type: FAILURE_ADD_EVENT,
    error: error
  }
}


export function deleteEvent(authorId, eventId) {
  return function (dispatch) {
    dispatch(requestDeleteEvent(event));
    return fetch(`${url}authors/${authorId}/events/${eventId}`, {
      method: 'DELETE',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
    }).then(
      data => data.json(),
      err => dispatch(failureDeleteEvent(err))
    ).then(
      events => dispatch(submitDeleteEvent(events))
    ).then(
      () => dispatch(fetchEvents(authorId))
    );
  }
}

export function requestDeleteEvent (authorId, eventId) {
  return {
    type: REQUEST_DELETE_EVENT,
    payload: {
      authorId,
      eventId
    }
  }
}

export function submitDeleteEvent (result) {
  return {
    type: SUBMIT_DELETE_EVENT,
    payload: result
  }
}

export function failureDeleteEvent (error) {
  return {
    type: FAILURE_DELETE_EVENT,
    payload: error
  }
}
