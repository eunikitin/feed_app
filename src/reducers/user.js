// @flow
import { combineReducers } from 'redux'

import {
  REQUEST_USERS,
  RECEIVE_USERS,
  FAILURE_USERS,

  REQUEST_CURRENT_USER,
  RECEIVE_CURRENT_USER,
  FAILURE_CURRENT_USER,
} from '../constants/user';


export function fetchUsers(state = {
  isFetching: false,
  payload: [],
}, action) {
  switch (action.type) {
    case REQUEST_USERS:
      return Object.assign({}, state, {
        isFetching: true
      });
    case RECEIVE_USERS:
      return Object.assign({}, state, {
        isFetching: false,
        payload: action.payload
      });
    case FAILURE_USERS:
      return Object.assign({}, state, {
        isFetching: false,
        error: action.payload
      });
    default:
      return state;
  }
}

export function fetchCurrentUser(state = {
  isFetching: false,
  payload: {},
}, action) {
  switch (action.type) {
    case REQUEST_CURRENT_USER:
      return Object.assign({}, state, {
        isFetching: true
      });
    case RECEIVE_CURRENT_USER:
      return Object.assign({}, state, {
        isFetching: false,
        payload: action.payload
      });
    case FAILURE_CURRENT_USER:
      return Object.assign({}, state, {
        isFetching: false,
        error: action.payload
      });
    default:
      return state;
  }
}

export default combineReducers({
  fetchUsers,
  fetchCurrentUser
});
