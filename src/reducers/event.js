import { combineReducers } from 'redux'

import {
  REQUEST_EVENTS,
  RECEIVE_EVENTS,
  FAILURE_EVENTS,

  REQUEST_ADD_EVENT,
  SUBMIT_ADD_EVENT,
  FAILURE_ADD_EVENT,

  REQUEST_DELETE_EVENT,
  SUBMIT_DELETE_EVENT,
  FAILURE_DELETE_EVENT
} from '../constants/event';

export function fetchEvents(state = {
  isFetching: false,
  payload: [],
}, action) {
  switch (action.type) {
    case REQUEST_EVENTS:
      return Object.assign({}, state, {
        isFetching: true
      });
    case RECEIVE_EVENTS:
      return Object.assign({}, state, {
        isFetching: false,
        payload: action.payload
      });
    case FAILURE_EVENTS:
      return Object.assign({}, state, {
        isFetching: false,
        error: action.payload
      });
    default:
      return state;
  }
}

export function addEvent(state = {
  isFetching: false,
  payload: false,
}, action) {
  switch (action.type) {
    case REQUEST_ADD_EVENT:
      return Object.assign({}, state, {
        isFetching: true
      });
    case SUBMIT_ADD_EVENT:
      return Object.assign({}, state, {
        isFetching: false,
        payload: action.payload
      });
    case FAILURE_ADD_EVENT:
      return Object.assign({}, state, {
        isFetching: false,
        error: action.payload
      });
    default:
      return state;
  }
}

export function deleteEvent(state = {
  isFetching: false,
  payload: false,
}, action) {
  switch (action.type) {
    case REQUEST_DELETE_EVENT:
      return Object.assign({}, state, {
        isFetching: true
      });
    case SUBMIT_DELETE_EVENT:
      return Object.assign({}, state, {
        isFetching: false,
        payload: action.payload
      });
    case FAILURE_DELETE_EVENT:
      return Object.assign({}, state, {
        isFetching: false,
        error: action.payload
      });
    default:
      return state;
  }
}

export default combineReducers({
  fetchEvents,
  addEvent,
  deleteEvent
});